#holds logging class, attribute dictionary constructors, url parsers, exception-to-log-file generator
import urlparse,threading,sys,Queue,time,os,traceback
if "/modules" not in sys.path[0] or "/modules" not in sys.path[-1]:
 path=os.path.realpath(__file__).rsplit(os.sep,1)[0]+"/modules"
 sys.path.insert(0,path)
import StringIO as stringio
Open=open
os_path_expanduser=os.path.expanduser
import web

def killAllThreadsExit(code=0):
 tl=threading.enumerate()
 for i in tl[1:]:
  die=[q for q in dir(i) if q.endswith("Q") or q.endswith("_q")]
#  print i
  for j in die:
   try:
    getattr(i,j).put(dict(name="exit"))
   except Exception,e:
    pass #print e

threadingExit=killAllThreadsExit

def filename(name):
 if not name.startswith(".") and not name.startswith("/"):
  name="~/.wb/"+name
 return os_path_expanduser(name)

def open(name="",mode="rb"):
 if not name.startswith(".") and not name.startswith("/"):
  name="~/.wb/"+name
 return Open(os_path_expanduser(name),mode)

def makeTemplate(name,vars):
 if not os.path.exists("templates/%s" % (name,)):
  return ""
 fh=Open("templates/%s" % (name,),"rb")
 fc=fh.read()
 fh.close()
 vList=[]
 bList=[]
 [[vList.append((i,v)) for i,v in j.items()] for j in vars]
 rList={}
 for i,v in vList:
  if i in bList:
   continue
  rList[i]=v if v!=None else ''
  bList.append(i)
  continue
 log("rList",str(rList))
 t=web.template.Template(fc,globals=rList,filename=name+".html")
 return str(t())

class zeroDict(dict):
 def __getattr__(self,x):
  try:
   return self[x]
  except KeyError:
   return 0

 def __setattr__(self,x,y):
  return self.__setitem__(x,y)

class attrDict(dict):
 def __getattr__(self,key):
  try:
   return self[key]
  except KeyError,e:
   raise AttributeError,e
 def __setattr__(self,key,val):
  z=self[key]=val
  return z
 def __delattr__(self,key):
  try:
   del self[key]
  except KeyError,e:
   raise AttributeError,e
 def __repr__(self):
  return "<AttrDict "+dict.__repr__(self)+">"

AttrDict=attrDict

def makeFullLink(url,root=""):
 log("makeFullLink, "+"url="+str(url)+", root="+str(root))
 x=urlparse.urljoin(root,url)
 if x.startswith("/") and ":" in root:
  while x.count("/")>2:
   x=x[:-1]
 if ".." in x:
  prot,d=x.split("//",1)
  dom,u=d.split("/",1)
  p=u.split("/")
  d=[]
  for i in range(len(p)):
   s=p[i]
   if s==".." and i==0:
    d.append(0)
   if s==".." and i>0:
    d.append(i-1); d.append(i)
   if s==".":
    d.append(i)
  d.sort()
  for i in d[::-1]:
   p.pop(i)
  x=prot+"//"+dom+"/"+"/".join(p)
 return x

def generate_error_report(e=None):
 fp = stringio.StringIO()
 traceback.print_exc(file=fp)
 message = fp.getvalue()
 del fp
 log(message)
 return message

class context(object):
 def __enter__(self,*l,**d):
  pass
 def __exit__(self,*l,**d):
  pass
 def __init__(self,*l,**kw):
  [setattr(self,k,v) for k,v in kw.items()]


class logger(threading.Thread):
 def __init__(self,**kw):
  threading.Thread.__init__(self)
  self.daemon=1
  [setattr(self,k,v) for k,v in kw.items()]
  self.logfh.write("log initialized at %s\n" % (time.asctime(),))
  self.logfh.flush()

 def fixme(self,x,suffix=1):
  s=""
  xt=type(x)
  if xt in (list,tuple):
   s+="["
   for i in x[:-1]:
    s+=self.fixme(i,suffix=0)+",\n"
   s+=self.fixme(x[-1],suffix=0)+"\n"
   s+="]"
  elif xt in (int,bool,str,unicode):
   s+=str(x).encode('utf-8')
   if suffix==1: s+="\n"
  elif xt==dict:
   y=x.items()
   y.sort()
   x=dict(y)
   s+="{\n"
   for k,v in x.items():
    s+=str(k)+": "+self.fixme(v,suffix=0)+"\n"
   s+="}\n$$$\n"
  else:
   s+=str(x)
  return s

 def run(self):
  self.logfh.write("running log loop\n")
  self.logfh.flush()
  while 1:
   time.sleep(0.001)
   try:
    i=self.q.get(block=0)
    if type(i)==dict and i.get("system",0)==1 and i.get("quit",0)==1:
     return 0
     self.logfh=open("/dev/null","wb")
    self.logfh.write(self.fixme(i))
    self.logfh.flush()
    if config.logging==99: self.outLogQ.put(i)
   except:
    pass
   try:
    self.in_q.get(block=0)
    return 0
   except Queue.Empty:
    pass

config=zeroDict(logging=1)
if config.logging==0:
 def log(*l):
  return
elif config.logging==99:
 def log(*l):
  for i in l:
   log_q.put(i)
   x=None if i!=None else 0
   while x!=i:
    time.sleep(0.001)
    try:
     x=outLogQ.get(block=0)
     if x!=i: time.sleep(0.1); log_q.put(x); x=None if i!=None else 0
    except:
     pass
else:
 def log(*l):
  [log_q.put(i) for i in l]

paths=["~/.wb/logs","~/.wb/downloads"]
[os.makedirs(os.path.expanduser(i)) for i in paths if not os.path.exists(os.path.expanduser(i))]
logfh=open(os.path.expanduser("~/.wb/logs/wb.err"),"wb")
sys.stderr=logfh
log_q=Queue.Queue()
outLogQ=Queue.Queue()
log_in_q=Queue.Queue()
lgr=logger(logfh=logfh,q=log_q,outLogQ=outLogQ,in_q=log_in_q)
lgr.start()


import socket
import code
import sys
class MyConsole(code.InteractiveConsole):
 def __init__(self, rfile, wfile, locals=None):
  self.rfile = rfile
  self.wfile = wfile
  code.InteractiveConsole.__init__(self, locals=locals, filename='<MyConsole>')

 def raw_input(self, prompt=''):
  self.wfile.write(prompt)
  return self.rfile.readline().rstrip()

 def write(self, data):
  self.wfile.write(data)

def debugger(port=7777,host='',locals={}):
 netloc = (host,port)
 servsock = socket.socket()
 servsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
 servsock.bind(netloc)
 servsock.listen(1)
 sock, _ = servsock.accept()
 rfile = sock.makefile('r', 0)
 sys.stdout = wfile = sock.makefile('w', 0)
 console = MyConsole(rfile, wfile,locals=locals)
 console.interact()

class box(dict,object):
 def __init__(self,**kw):
  log("class:init:box")
  [setattr(self,k,v) for k,v in kw.items()]

 def __setattr__(self,x,y):
  self[x]=y

 def __getattr__(self,x):
  return self[x]

