#file with bookmarks class, unimpl as of 9/4/12
import web
class bookmarks(object):
 def __init__(self,filename="bookmarks.db"):
  filename=utils.Filename(filename)
  self.db=web.database(db=filename,dbn="sqlite")
  db.query("create table if not exists bookmarks (id integer primary key, title text, url text not null, parent int not null)")
  db.query("create table if not exists bookmark_folders (id integer primary key, name text not null, parent int not null)")

 def addBookmark(self,url="",title="",parent=0):
  self.db.insert("bookmarks",url=url,title=title,parent=parent)

 def addBookmarkFolder(self,name="",parent=0):
  self.db.insert("bookmark_folders",name=name,parent=parent)

 def listBookmarks(self,parent=0):
  return list(db.query("select id,url,title from bookmarks where parent=$parent",{"parent":parent}))

 def listFoldersInParent(self,parent):
  return list(self.db.query("select id,name from bookmark_folders where parent=$parent",{"parent":parent}))

 def listBookmarksByFolder(self,parent=0,level=0):
  d={}
  folders=self.listFoldersInParent(parent)
  for i in folders:
   d[i['name']]=self.listBookmarksByFolder(i['id'])
  urls=self.listBookmarks(parent)
  for i in urls:
   d[i['url']]=d['title'] if d['title'].strip()!="" else i['url']
  return d

