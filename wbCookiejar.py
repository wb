#cookiejar for browser, stores all cookies in SQLite format
import sys
import web,utils,cookielib,threading
web.config.debug=0

class sqliteCookiejar(cookielib.CookieJar):
 def __init__(self,filename="cookies.sqlite",policy=None):
  self._cookies_lock=threading.Lock()
  filename=utils.filename(filename)
  cookielib.CookieJar.__init__(self,policy)
#self._cookies_lock
  self.db=web.database(dbn="sqlite",db=filename,check_same_thread=False)
  self.db.query("create table if not exists cookies (id integer primary key, name text, value text, domain text, path text, expires integer, discard bool, httpOnly bool, secure bool)")

 def __getstate__(self):
  with self._cookies_lock:
   x=[dict(i) for i in self.db.query("select * from cookies")]
  return x

 def __setstate__(self,x):
  for i in x:
   self.set_cookie(i)

 def dbToCookie(self,d):
  version=0
  keys="name","value","domain","path"
  [d.update([(k,d[k].encode('ascii','ignore'))]) for k in keys]
  d['secure']=bool(d['secure'])
  rest={}
  if d['httpOnly']: rest['HttpOnly']=None
  domain_specified=initial_dot=d['domain'].startswith(".")
  d['discard']=0 if d['expires']=="" else 1
  if d['expires']=="": d['expires']=None
  return cookielib.Cookie(version,d['name'],d['value'],None,False,d['domain'],domain_specified,initial_dot,d['path'],False,d['secure'],d['expires'],d['discard'],None,None,rest)

 def cookieToDb(self,c):
  d={}
  keys="name","value","domain","path"
  [d.update([(k,unicode(getattr(c,k)))]) for k in keys]
  d['expires']=c.expires if not c.discard else ""
  d['discard']=c.discard
  d['secure']=bool(int(c.secure))
  d['httpOnly']=c.has_nonstandard_attr("HttpOnly")
  return d

 def set_cookie(self,c):
  d=self.cookieToDb(c) if not hasattr(c,"items") else c
  with self._cookies_lock:
   ret=list(self.db.query("select count(*) as cnt from cookies where name=$name and domain=$domain and path=$path",d))[0]['cnt']
   if ret:
    self.db.update("cookies",where='name=$name and domain=$domain and path=$path',vars=d,**d)
   else:
    self.db.insert("cookies",**d)
  return 1

 @property
 def _cookies(self):
  with self._cookies_lock:
   x=[self.dbToCookie(i) for i in self.db.query("select * from cookies order by domain asc, path asc, name asc")]
  return x

 def __iter__(self):
  with self._cookies_lock:
   res=list(self.db.query("select * from cookies order by domain asc, path asc, name asc"))
  for i in res:
   yield self.dbToCookie(i)

 def _cookies_for_request(self,request):
  cookies=[]
  domains=[i['domain'] for i in self.db.query("select domain from cookies group by domain")]
  with self._cookies_lock:
   for d in domains:
    if self._policy.domain_return_ok(d,request):
     res=[self.dbToCookie(i) for i in self.db.query("select * from cookies where domain=$domain",{"domain":d})]
     [cookies.append(i) for i in res if self._policy.path_return_ok(i.path,request)]
  return cookies

 def clear(self,domain,path,name):
  if name!=None and (domain==None or path==None):
   raise valueError("domain and path are required to clear cookies by name")
  elif path!=None and domain==None:
   raise valueError("domain is required to clear cookies by path")
  else:
   pass
  with self._cookies_lock:
   if domain==path==name==None:
    self.db.query("delete from cookies")
   elif path==name==None:
    self.db.query("delete from cookies where domain=$domain",{"domain":domain})
   elif name==None:
    self.db.query("delete from cookies where domain=$domain and path=$path",{"domain":domain,"path":path})
   else:
    self.db.query("delete from cookies where domain=$domain and path=$path and name=$name",{"domain":domain,"path":path,"name":name})
