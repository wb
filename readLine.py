import string
class readline(object):
 wordChars=string.letters+string.digits

 @property
 def lt(self):
  return len(self.text)

 def __init__(self,**kw):
  self._mode="insert"
  self.offset=0
  self.text=[]
  self.origText=None
  [setattr(self,k,v) for k,v in kw.items()]
  self.historyLocation=len(self.history)
  self.text=list(self.text)
  self.pos=0
  self.start=len(self.prompt)

 def _print(self):
  self.screen.move(self.line,0)
  self.screen.clrtoeol()
  self.screen.adstr(prompt.rstrip(":",1)+":")
  if self.pos+self.start>self.maxx:
   m=self.maxx-len(self.prompt)-1
   if self.pos==len(self.text):
    s,e=-1*m,None
   else:
    s,e=self.pos,self.pos
    while 1:
     if s>0:
      s-=1
     if e-s>=len(self.prompt):
      break
     if e<len(m):
      e+=1
     if e-s>=len(self.prompt):
      break
   p="$"+"".join(self.text[s:e])
  else:
   p=self.prompt+"".join(self.text)
  self.screen.adstr(self.line,0,p)

 def insertChar(self,c):
  c=chr(c) if type(c)==int else c
  self.text.insert(self.pos,c)
  self.pos+=1

 def backwardDeleteChar(self):
  if self.pos>0: self.text=self.text[:self.pos-1]+self.text[self.pos:]

 def cursorRight(self):
  if self.pos>self.lt:
   return -1
  self.pos+=1

 def cursorLeft(self):
  if self.pos==0:
   return -1
  self.pos-=1

 def curDeleteChar(self):
  if self.pos<self.lt: self.text=self.text[:self.pos]+self.text[self.pos+1:]

 def beginningOfLine(self):
  self.pos=0

 def endOfLine(self):
  self.pos=len(self.text)

 def backwardOneWord(self):
  if self.text[self.pos] in self.wordChars:
   while self.pos>0:
    self.pos-=1
    if self.pos not in self.wordChars:
     self.pos+=1
     return self.pos
  if self.text[self.pos] not in self.wordChars:
   while self.pos>0 and self.text[self.pos] not in self.wordChars:
    self.pos-=1
   while self.pos>0:
    self.pos-=1
    if self.pos not in self.wordChars:
     self.pos+=1
     return self.pos

 def forwardOneWord(self):
  if self.text[self.pos] in self.wordChars:
   while self.pos<len(self.text):
    self.pos+=1
    if self.pos not in self.wordChars:
     self.pos-=1
     return self.pos
  if self.text[self.pos] not in self.wordChars:
   while self.pos<len(self.text) and self.text[self.pos] not in self.wordChars:
    self.pos+=1
   while self.pos<len(self.text):
    self.pos+=1
    if self.pos not in self.wordChars:
     self.pos-=1
     return self.pos

 def clearScreen(self):
  self.redrawCurrentLine()

 def redrawCurrentLine(self):
  pass

 def acceptLine(self):
  self.done=1

 def previousHistory(self):
  if self.origText==None: self.history.append(self.text); self.origText=self.text
  self.histLoc-=1 if self.histLoc>0 else 0
  self.text=self.history[self.histLoc]

 def nextHistory(self):
  if self.origText==None: self.history.append(self.text); self.origText=self.text
  self.histLoc+=1 if self.histLoc<len(self.hist) else 0
  self.text=self.history[histLoc]

 def beginningOfHistory(self):
  self.histLoc=0
  self.text=self.history[histLoc]

 def endOfHistory(self):
  self.histLoc=len(self.history)-1
  self.text=self.history[histLoc]

 def tabInsert(self):
  self.insertChar("\t")

 def quotedInsert(self):
  c=self.screen.getch()

 def deleteChar(self):
  if self.pos==len(self.text):
   return -1
  self.text=self.text[:self.pos],self.text[self.pos:]

 def forwardBackwardDeleteChar(self):
  if self.pos==len(self.text):
   self.backwardDeleteChar()
  else:
   self.forwardDeleteChar()

 def transposeChars(self):
  if self.pos==self.lt:
   a,b=self.text[-2:]
   self.text[-2:]=b,a
  else:
   self.pos=1
   a,b=self.pos-1,self.pos
   sa,sb=self.text[a:b]
   self.text[a:b]=sb,sa

 def mode(self,arg=-1):
  if arg>=0:
   self._mode="insert"
  else:
   self._mode="overwrite"

 def killLine(self):
  self.text,t=self.text[:self.pos],self.text[self.pos:]
  self.killRing.append(t)

 def backwardKillLine(self):
  t,self.text=self.text[:self.pos],self.text[self.pos:]
  self.killRing.append(t)

 unixLineDiscard=backwardKillLine

 def killWholeLine(self):
  self.killRing.append("".join(self.text))
  self.text=[]

 def killWord(self):
  while self.text[self.pos]!=self.wordChars:
   self.text=self.text[:self.pos]+self.text[self.pos+1:]
  t=""
  while self.text[self.pos] in self.wordChars:
   t+=self.text.pop(self.pos)
  self.killRing.append(t)

 def backwardKillWord(self):
  while self.text[self.pos]!=self.wordChars:
   self.text=self.text[:self.pos-1]+self.text[self.pos:]
  t=""
  while self.text[self.pos] in self.wordChars:
   t+=self.text.pop(self.pos-1)
   self.pos-=1
  self.killRing.append(t)

 def unixWordRubout(self):
  t=""
  while self.text[self.pos-1] not in string.whitespace:
   t+=self.text.pop(self.pos-1)
   self.pos-=1
  self.killRing.append(t)
 def unixFilenameRubout(self):
  t=""
  while self.text[self.pos-1] not in string.whitespace+"/":
   t+=self.text.pop(self.pos-1)
   self.pos-=1
  self.killRing.append(t)
 def deleteHorizontalSpace(self):
  while self.text[self.pos+1] in string.whitespace:
   self.text.pop(self.pos+1)
  while self.text[self.pos-1] in string.whitespace:
   self.text.pop(self.pos-1)
   self.pos-=1
#paste
 def yank(self):
  t=self.killRing[-1]
  [self.text.insert(self.pos,i) for i in t[::-1]]
 def yankPop(self):
  self.killRing.insert(0,self.killRing.pop(-1))
  t=self.killRing.pop(-1)
  [self.text.insert(self.pos,i) for i in t[::-1]]
 def complete(self):
  match=self.text[:self.pos]
  matchRes=[i for i in self.history if i.lower().startswith(match.lower())]
  if len(matchRes)==1:
   [self.insertChar(i) for i in matchRes[0]]
   return
  if matchRes:
   if self.lastCommand=="complete":
    self.possibleCompletions()

 def possibleCompletions(self):
  return

 def menuCompleteBackward(self):
  self.menuComplete(-1)

 def menuComplete(self,arg=1):
  match=self.text[:self.pos]
  if self.lastCommand not in ["menuComplete","menuCompleteBackward"]:
   self.matchRes=[i for i in self.history if i.lower().startswith(match.lower())]
   self.matchIndex=0
   self.matchText=match
  if not self.matchRes:
   self.bell()
   return
  if arg>-1:
   self.matchIndex+=1
  else:
   self.matchIndex-=1
  if self.matchIndex<0:
   self.matchIndex=len(self.matchRes)-1
  if self.matchIndex>=len(self.matchRes):
   self.matchIndex=0
  self.text=self.matchText+self.matchRes[self.matchIndex]

 def possibleCompletions(self):
  match=self.text[:self.pos]
  matchRes=[i for i in self.history if i.lower().startswith(match.lower())]

 def insertCompletions(self):
  match=self.text[:self.pos]
  matchRes=[i for i in self.history if i.lower().startswith(match.lower())]
  matchRes=" ".join([i.strip() for i in matchRes])
  [self.insertChar(i) for i in matchRes]

 def loop(self):
  self._print()
  while 1:
   k=self.screen.getch()
   if 31<k<128:
    self.insertChar(chr(k))
    self._print()
    continue
   x=self.screen.getctrl(k)
   if x.startswith("M-") and x.split("-",1)[-1].isdigit():
    self.numArgs.append(x.split("-",1)[-1])
    continue
   if x in self.keyBindings:
    getattr(self,self.keyBindings[x])(int("".join(self.numArgs)) if self.numArgs else None)
    self._print()

