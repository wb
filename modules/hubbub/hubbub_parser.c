//#ifdef xx
#include <Python.h>
//#endif
#include <assert.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <hubbub/parser.h>
#include <hubbub/tree.h>
#include "hubbub_parser.h"
#define UNUSED(x) ((x)=(x))
typedef enum error_code { OK, NOMEM, BADENCODING, ENCODINGCHANGE } error_code;
typedef enum encoding_source { ENCODING_SOURCE_HEADER, ENCODING_SOURCE_DETECTED, ENCODING_SOURCE_META } encoding_source;
typedef struct context {
hubbub_parser *parser;
Node *document;
const char *encoding;			/**< The charset of the input */
encoding_source enc_source;		/**< The encoding source */
hubbub_tree_handler tree_handler;	/**< Hubbub tree callbacks */
} context;
error_code create_context(const char *charset, context **ctx);
void destroy_context(context *c);
error_code parse_chunk(context *c, const uint8_t *data, size_t len);
error_code parse_completed(context *c);
static void countNodes(Node *, int *);
static Node * destroy_partial_context(context *);
static Node * lastChild(Node *);
static Node * appendChild(Node *, Node *);
static Node * newNode(char *, char *, char *, char *, Node *, Node *, bool);
static Node * newSimpleNode(char *, char *, char *);
static inline char * strnull(const char *s);
static inline char * strnnull(const char *s, int size);
static Node *addAttribute(Node *n, char *name, char *value);
//static void print (char *s);
static hubbub_error create_comment(void *ctx, const hubbub_string *data, void **result);
static hubbub_error create_doctype(void *ctx, const hubbub_doctype *doctype,void **result);
static hubbub_error create_element(void *ctx, const hubbub_tag *tag, void **result);
static hubbub_error create_text(void *ctx, const hubbub_string *data, void **result);
static hubbub_error ref_node(void *ctx, void *node);
static hubbub_error unref_node(void *ctx, void *node);
static hubbub_error append_child(void *ctx, void *parent, void *child, void **result);
static hubbub_error insert_before(void *ctx, void *parent, void *child, void *ref_child, void **result);
static hubbub_error remove_child(void *ctx, void *parent, void *child, void **result);
static hubbub_error clone_node(void *ctx, void *node, bool deep, void **result);
static hubbub_error reparent_children(void *ctx, void *node, void *new_parent);
static hubbub_error get_parent(void *ctx, void *node, bool element_only, void **result);
static hubbub_error has_children(void *ctx, void *node, bool *result);
static hubbub_error form_associate(void *ctx, void *form, void *node);
static hubbub_error add_attributes(void *ctx, void *node, const hubbub_attribute *attributes, uint32_t n_attributes);
static hubbub_error set_quirks_mode(void *ctx, hubbub_quirks_mode mode);
//static hubbub_error change_encoding(void *ctx, const char *charset);
hubbub_tree_handler tree_handler =
{
create_comment,
create_doctype,
create_element,
create_text,
ref_node,
unref_node,
append_child,
insert_before,
remove_child,
clone_node,
reparent_children,
get_parent,
has_children,
form_associate,
add_attributes,
set_quirks_mode,
NULL, //change_encoding,
NULL
};

Node * insertBefore (Node *new, Node *old)
{
//print("insertBefore");
Node *current;
//fprintf(stdout,"inserting %s/%s before %s/%s\n",old->type,old->name,new->type,new->name);
new->parent=old->parent;
new->next=old;
if (old->parent->children==old)
{
old->parent->children=new;
} else {
current=old->parent->children;
while (current)
{
if (current->next==old)
{
current->next=new;
break;
}
current=current->next;
}
}
return new;
}
Node * copyNode(Node *old, Node *parent, bool deep)
{
//print("copyNode");
Node *new,*current;
//fprintf(stdout,"copying %s/%s\n",old->type,old->name);
new=newNode(old->type,old->name,old->value,old->extra,NULL,NULL,1);
if (parent) {
  appendChild(new,parent);
 }
if (!deep) {
  return new;
 }
current=old->children;
while (current) {
  copyNode(current,new,deep);
  current=current->next;
 }
return new;
}

Node * appendChild(Node *new, Node *parent)
{
//print("appendChild");
//fprintf(stdout,"appending %s/%s\n",new->type,new->name);
new->parent=parent;
if (parent->children==NULL) {
  parent->children=new;
  parent->lastChild=new;
 } else {
if (!parent->lastChild && parent->children)
{
parent->lastChild=lastChild(parent);
}
  parent->lastChild->next=new;
  new->next=NULL;
  parent->lastChild=new;
 }
return new;
}

Node * prependChild(Node *new, Node *parent)
{
//print("prependChild");
//fprintf(stdout,"prepending %s/%s\n",new->type,new->name);
Node *first;
first=parent->children;
if (first==NULL) {
  parent->children=new;
  new->parent=parent;
  parent->lastChild=new;
 } else {
  parent->children=new;
  new->next=first;
  new->parent=parent;
 }
return new;
}

Node * unlinkNode(Node *n)
{
//print("unlinkNode");
//fprintf(stdout,"unlinking %s/%s\n",n->type,n->name);
Node *prev,*current;
current=n->parent->children;
while (current)
{
prev=current;
current=current->next;
if (current==n)
{
prev->next=current->next;
break;
}
}
n->parent->lastChild=lastChild(n->parent);
n->parent=NULL;
n->attributes=NULL;
n->lastChild=NULL;
n->next=NULL;
return n;
}

Node * lastChild(Node *parent)
{
Node *current;
current = parent->children;
while (current->next) {
  current=current->next;
 }
return current;
}

static char * strnull(const char *s)
{
if (s==NULL)
{
return NULL;
}
return strdup(s);
}
static char * strnnull(const char *s, int size)
{
if (s==NULL) {
  return NULL;
 }
return strndup(s,size);
}

void *myrealloc(void *ptr, size_t len, void *pw)
{
return realloc(ptr, len);
}

Node * parseString(char *s,long l)
{
error_code error;
context *c;
uint8_t *buf;
size_t len;
len=l;
buf = malloc(len+1);
if (buf == NULL) {
  return NULL;
 }
strncpy((char *)buf,s,len+1);
error = create_context(NULL, &c);
if (error != OK) {
  free(buf);
  return NULL;
 }
error = parse_chunk(c, buf, len);
if (1==0) {
  if (error == ENCODINGCHANGE) {
    hubbub_parser *temp;
    if (hubbub_parser_create(c->encoding, true, myrealloc, NULL, &temp) != HUBBUB_OK) {
      destroy_context(c);
      free(buf);
      return NULL;
    }
    hubbub_parser_destroy(c->parser);
    c->parser = temp;
    error = parse_chunk(c, buf, len);
  }
  if (error != OK) {
    destroy_context(c);
    free(buf);
    return NULL;
  }
 }
if (error!=OK) {
  //fprintf(stdout,"error\n");
 }
error = parse_completed(c);
if (error != OK) {
  destroy_context(c);
  free(buf);
  return NULL;
 }
free(buf);
//print("deleting it all for cleanup.");
return destroy_partial_context(c);
}

Node * newSimpleNode(char *type, char *name, char *value)
{
return (Node *) newNode(type,name,value,NULL,NULL,NULL,1);
}
Node * newNode(char *type, char *name, char *value,char *extra, Node *parent, Node *child,bool allocate)
{
Node *n=malloc(sizeof(Node));
if (allocate==1)
{
n->name=strnull(name);
n->value=strnull(value);
n->type=strnull(type);
n->extra=strnull(extra);
} else {
n->name=name;
n->value=value;
n->type=type;
n->extra=extra;
}
n->next=NULL;
n->children=child;
n->parent=parent;
n->refcount=0;
return n;
}
Node *addAttribute(Node *n, char *name, char *value)
{
Node *new=newNode("attribute",name,value,NULL,NULL,NULL,1);
new->parent=n;
prependChild(new,n);
return n;
}
void deleteNode(Node *n, int children)
{
if (children>0)
{
Node *current;
current=n->children;
while (current)
{
Node *temp;
temp=current->next;
deleteNode(current,1);
current=temp;
}
}
free(n->type);
free(n->name);
free(n->value);
free(n->extra);
if (n->parent)
{
unlinkNode(n);
}
n->parent=NULL;
n->next=NULL;
n->lastChild=NULL;
free(n);

if (1==0)
{
if (children<2 && n->parent)
{
if (n->parent->children==n)
{
n->parent->children=n->next;
}
if (n->parent->lastChild==n && n->parent->children!=NULL)
{
Node *current;
current=n->parent->children;
while (current)
{
if (current->next==n)
{
current->parent->lastChild=current;
break;
}
}
} else {
n->parent->lastChild=NULL;
}
}
}
}

/*
void print(char *msg)
{
fprintf(stdout,"%s\n",msg);
fflush(stdout);
}
*/

error_code create_context(const char *charset, context **ctx)
{
context *c;
hubbub_parser_optparams params;
hubbub_error error;
Node *n;
c = malloc(sizeof(context));
if (c == NULL)
{
return NOMEM;
}
c->parser = NULL;
c->encoding = charset;
c->enc_source = ENCODING_SOURCE_HEADER;
c->document = NULL;
error = hubbub_parser_create(c->encoding, true, myrealloc, NULL, &c->parser);
if (error != HUBBUB_OK)
{
free(c);
if (error == HUBBUB_BADENCODING)
{
return BADENCODING;
}		else {
return NOMEM;	/* Assume OOM */
}
}
n=newNode("#root",NULL,NULL,NULL,NULL,NULL,1);
c->document = (void *) n;
if (c->document == NULL) {
hubbub_parser_destroy(c->parser);
free(c);
return NOMEM;
}
c->tree_handler = tree_handler;
c->tree_handler.ctx = (void *) c;
//params.enable_scripting=0;
params.tree_handler = &c->tree_handler;
hubbub_parser_setopt(c->parser, HUBBUB_PARSER_TREE_HANDLER, &params);
ref_node(c, c->document);
params.document_node = c->document;
hubbub_parser_setopt(c->parser, HUBBUB_PARSER_DOCUMENT_NODE, &params);
//hubbub_parser_setopt(c->parser, HUBBUB_PARSER_ENABLE_SCRIPTING, &params);
*ctx = c;
return OK;
}
Node * destroy_partial_context(context *c)
{
if (c == NULL)
return NULL;
Node *doc=c->document;
if (c->parser != NULL)
{
hubbub_parser_destroy(c->parser);
}
c->parser = NULL;
c->encoding = NULL;
free(c);
return doc;
}
void destroy_context(context *c)
{
if (c == NULL)
return;
if (c->parser != NULL)
{
hubbub_parser_destroy(c->parser);
}
c->parser = NULL;
c->encoding = NULL;
deleteNode(c->document,1);
c->document = NULL;
free(c);
return;
}
error_code parse_chunk(context *c, const uint8_t *data, size_t len)
{
hubbub_error err;
err = hubbub_parser_parse_chunk(c->parser, (uint8_t *) data, len);
if (err == HUBBUB_ENCODINGCHANGE)
{
return ENCODINGCHANGE;
}
return OK;
}
error_code parse_completed(context *c)
{
hubbub_error error;
error = hubbub_parser_completed(c->parser);
if (error!=HUBBUB_OK)
{
//print("nomem,completed");
return HUBBUB_NOMEM;
}
return OK;
}

hubbub_error create_comment(void *ctx, const hubbub_string *data, void **result)
{
//print("create_comment");
Node *n;
char *content;
content = strnnull((const char *)data->ptr,data->len);
n=newSimpleNode("comment",NULL,NULL);
n->value=content;
if (n == NULL) {
  free(content);
  return HUBBUB_NOMEM;
 }
n->refcount=1;
*result = (void *) n;
return HUBBUB_OK;
}

hubbub_error create_doctype(void *ctx, const hubbub_doctype *doctype, void **result)
{
//print("create_doctype");
Node *n;
char *name=NULL, *public = NULL, *system = NULL;
name = strnnull((const char *)doctype->name.ptr,doctype->name.len);
if (name == NULL) {
  return HUBBUB_NOMEM;
 }
if (!doctype->public_missing) {
  public = strnnull((const char *)doctype->public_id.ptr, doctype->public_id.len);
  if (public == NULL) {
    free(name);
    return HUBBUB_NOMEM;
  }
 }
if (!doctype->system_missing) {
  system = strnnull((const char *)doctype->system_id.ptr, doctype->system_id.len);
  if (system == NULL) {
    free(public);
    free(name);
    return HUBBUB_NOMEM;
  }
 }
n=newSimpleNode("doctype",NULL,NULL);
if (n == NULL) {
  free(system);
  free(public);
  free(name);
  return HUBBUB_NOMEM;
 }
n->name=name;
n->value=public;
n->extra=system;
n->refcount=1;
*result = (void *) n;
return HUBBUB_OK;
}

hubbub_error create_element(void *ctx, const hubbub_tag *tag, void **result)
{
//print("create_element");
int error;
char *name;
Node *n;
name = strnnull((const char *)tag->name.ptr,tag->name.len);
if (name == NULL) {
  return HUBBUB_NOMEM;
 }
n=newSimpleNode("start",NULL,NULL);
if (n == NULL) {
  //print("err:nomem");
  return HUBBUB_NOMEM;
 }
//print(name);
n->name=name;
n->refcount=1;
error=0;
if (tag->n_attributes > 0) {
 int aIndex=0;
 for (aIndex=0; aIndex<tag->n_attributes; aIndex++) {
   char *name=strnnull((const char *)tag->attributes[aIndex].name.ptr,tag->attributes[aIndex].name.len);
   char *value=strnnull((const char *)tag->attributes[aIndex].value.ptr,tag->attributes[aIndex].value.len);
   addAttribute(n,name,value);
  }
 }
if (error==1) {
  //print("error==1, deleting");
  deleteNode(n,1);
  free(name);
  return HUBBUB_NOMEM;
 }
*result = (void *) n;
return HUBBUB_OK;
}

hubbub_error create_text(void *ctx, const hubbub_string *data, void **result)
{
//print("create_text");
Node *n;
n=newSimpleNode("text",NULL,NULL);
if (n == NULL) {
  return HUBBUB_NOMEM;
 }
n->value=strnnull((const char *)data->ptr,data->len);
n->refcount=1;
*result = (void *) n;
return HUBBUB_OK;
}

hubbub_error append_child(void *ctx, void *parent, void *child, void **result)
{
//print("append_child");
Node *c,*p;
p=parent;
c=child;
*result=appendChild(c,p);
if (*result == NULL)
{
return HUBBUB_NOMEM;
}
ref_node(ctx, *result);
return HUBBUB_OK;
}
hubbub_error insert_before(void *ctx, void *parent, void *child, void *ref_child, void **result)
{
//print("insert_before");
Node *ch,*refCh,*p;
refCh=ref_child;
ch=child;
p=parent;
if (p!=refCh->parent)
{
//print("error:parent != child in insert_before");
}
if (p!=NULL)
{
*result=insertBefore(ch,refCh);
}
if (*result == NULL)
{
return HUBBUB_NOMEM;
}
ref_node(ctx, *result);
return HUBBUB_OK;
}
hubbub_error remove_child(void *ctx, void *parent, void *child, void **result)
{
//print("remove_child");
Node *p,*c;
p=parent;
c=child;
if (p!=c->parent)
{
//print("parent!=child->parent in remove_child");
return HUBBUB_NOMEM;
}
*result=unlinkNode(c);
ref_node(ctx, *result);
return HUBBUB_OK;
}
hubbub_error clone_node(void *ctx, void *node, bool deep, void **result)
{
//print("clone_node");
*result=copyNode(node,NULL,deep);
if (*result == NULL)
{
return HUBBUB_NOMEM;
}
return HUBBUB_OK;
}
hubbub_error reparent_children(void *ctx, void *node, void *new_parent)
{
//print("reparent_children");
Node *src,*dst,*current,*temp;
src=node;
dst=new_parent;
current=src->children;
while (current)
{
temp=unlinkNode(current);
appendChild(temp,dst);
current=current->next;
}
return HUBBUB_OK;
}
hubbub_error get_parent(void *ctx, void *node, bool element_only, void **result)
{
//print("get_parent");
context *c=(context *) ctx;
Node *n;
n=node;
if (element_only==1 && strcmp(n->parent->type,"start")!=0)
{
*result=NULL;
} else {
*result=n->parent;
if (*result!=NULL)
{
ref_node(c,*result);
}
}
*result=(void *) *result;
return HUBBUB_OK;
}
hubbub_error has_children(void *ctx, void *node, bool *result)
{
//print("has_children");
Node *n;
n=node;
*result=(n->children==NULL) ? 0 : 1;
return HUBBUB_OK;
}
hubbub_error add_attributes(void *ctx, void *node, const hubbub_attribute *attributes, uint32_t n_attributes)
{
//print("add_attributes");
Node *n;
n=node;
int aIndex=0;
for (aIndex=0; aIndex<n_attributes; aIndex++) {
  char *name=strnnull((const char *)attributes[aIndex].name.ptr,attributes[aIndex].name.len);
  char *value=strnnull((const char *)attributes[aIndex].value.ptr,attributes[aIndex].value.len);
  addAttribute(n,name,value);
 }
return HUBBUB_OK;
}

hubbub_error ref_node(void *ctx, void *node)
{
//print("ref_node");
Node *n;
n=node;
n->refcount+=1;
return HUBBUB_OK;
}

hubbub_error unref_node(void *ctx, void *node)
{
//print("unrefNode");
Node *n;
n=node;
n->refcount-=1;
if (n->refcount==0 && n->parent==NULL && strcmp(n->type,"#root")!=0) {
  deleteNode(n,1);
 }
return HUBBUB_OK;
}

hubbub_error form_associate(void *ctx, void *form, void *node)
{
return HUBBUB_OK;
}
hubbub_error set_quirks_mode(void *ctx, hubbub_quirks_mode mode)
{
return HUBBUB_OK;
}

/*
hubbub_error change_encoding(void *ctx, const char *charset)
{
context *c = (context *) ctx;
uint32_t source;
if (c->encoding != NULL) {
  return HUBBUB_OK;
 }
if (source == HUBBUB_CHARSET_CONFIDENT) {
  c->enc_source = ENCODING_SOURCE_DETECTED;
  c->encoding = (char *) charset;
  return HUBBUB_OK;
 }
c->encoding = charset;
c->enc_source = ENCODING_SOURCE_META;
return HUBBUB_OK;
}
*/

int main(int argc, char **argv)
{
if (argc<2) {
  fprintf(stderr,"usage:parser filename\n");
  return 1;
 }
char *filename=argv[1];
FILE *fp=fopen(filename,"rb");
fseek(fp,0,2);
long size=ftell(fp);
char *s=malloc(size+1);
fseek(fp,0,0);
fread(s,1,size,fp);
s[size]='\0';
fclose(fp);
//fprintf(stdout,"file:%d,s:%d\n",size,strlen(s));
Node *ret=parseString(s,size);
int x=0;
countNodes(ret,&x);
//fprintf(stdout,"total:%l\n",x);
return 0;
}

void countNodes(Node *n, int *x)
{
Node *current;
current = n->children;
while (current) {
  //fprintf(stdout,".%d\n",*x);
  countNodes(current,x++);
  current=current->next;
 }
return;
}

//#ifdef xx
static void getFullNodeList(Node *, PyObject *);
void getFullNodeList(Node *n, PyObject *l)
{
Node *current;
PyObject *t;
t=Py_BuildValue("(zzzz)",n->type,n->name,n->value,n->extra);
PyList_Append(l,t);
Py_DECREF(t);
current=n->children;
while (current)
{
getFullNodeList(current,l);
current=current->next;
}
if (strncmp(n->type,"text",4)!=0 && strncmp(n->type,"attribute",9)!=0 && strncmp(n->type,"doctype",7)!=0 && strncmp(n->type,"comment",7)!=0)
{
t=Py_BuildValue("(zzzz)","end",n->name,n->value,n->extra);
PyList_Append(l,t);
Py_DECREF(t);
}
}

static PyObject *pyParseString(PyObject *self, PyObject *args)
{
Node *doc;
PyObject *containerList;
char *s;
long len;
if (!PyArg_ParseTuple(args,"s#",&s,&len))
{
return NULL;
}
doc=parseString(s,len);
//do normal work here, e.g. itterate through the provided charArPtr, much faster than python
containerList=PyList_New(0);
getFullNodeList(doc,containerList);
deleteNode(doc,1);
return containerList;
//PyObject *newName=Py_BuildValue("(sss)",ptr1,ptr2,ptr3);
//PyTuple_SetItem(name,index,newName);
//return name;
}

static PyMethodDef parser_newMethods[] = {
{"pyParseString", pyParseString, METH_VARARGS, "Turn HTML document into (type,name,value) tripples"},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};
PyMODINIT_FUNC init_hubbub_parser(void)
{
    (void) Py_InitModule("_hubbub_parser", parser_newMethods);
//fprintf(stdout,"init pymod\n");
}
//#endif
