#fixes lack of address caching in some python installs (note that cached addresses do not expire until the program exits)
import socket
socket.ghbn=socket.gethostbyname
socket.ghbn_ex=socket.gethostbyname_ex
socket_gethostbyname_res={}
def gethostbyname(x):
 global socket_gethostbyname_res
 if x not in socket_gethostbyname_res:
  ret=socket.ghbn(x)
  socket_gethostbyname_res[x]=ret
  return ret
 return socket_gethostbyname_res[x]
def gethostbyname_ex(x):
 return gethostbyname(x)
socket.gethostbyname=gethostbyname
socket.gethostbyname_ex=gethostbyname_ex

